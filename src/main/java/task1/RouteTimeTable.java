package task1;

/**
 * Created by Ira on 21.11.2015.
 */
public class RouteTimeTable {
    private int id;

    private int routeId;
    private int stationId;
    private int transportId;
    private int time;
    private String direction;

    public RouteTimeTable(int id, int routeId, int stationId, int transportId, int time, String direction ){
        this.id = id;
        this.routeId = routeId;
        this.transportId = transportId;
        this.stationId = stationId;
        this.time = time;
        this.direction = direction;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public int getStationId() {
        return stationId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    public int getTransportId() {
        return transportId;
    }

    public void setTransportId(int transportId) {
        this.transportId = transportId;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }



}
