package task1;


import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.sql.*;

/**
 * Created by Ira on 21.11.2015.
 */
public class TestJDBC {


    public static void main(String[] args) {

        int size;

        System.out.println("Copyright 2015, Irina Spiridonova");
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/transport";
            con = DriverManager.getConnection(url, "root", "root");
            stmt = con.createStatement();

            //            new ManagerTransport(con);
//            new ManagerRoute(con).getAllRoute();
//            new ManagerStation(con);
            ManagerRouteStation mrs = new ManagerRouteStation(con);
            ManagerStation ms = new ManagerStation(con);
            ManagerTransport tr = new ManagerTransport(con);
            size = mrs.route(1).size();
            int geoN = 0;
            int geoW = 0;
            double distance;


            for (int i = 1; i < size; i++) {
                int a = mrs.route(1).get(i).getStationId();
                int b = mrs.route(1).get(i - 1).getStationId();
                geoN = ms.geoStation(b).getGeoN();
                geoW = ms.geoStation(mrs.route(1).get(i - 1).getStationId()).getGeoW();
                distance = Math.sqrt(Math.sqrt(Math.abs(geoN - ms.geoStation(a).getGeoN())) + Math.sqrt(Math.abs(geoW - ms.geoStation(a).getGeoW())));
                System.out.println((int)Math.round(distance));
                tr.transport(1).getSpeed();

            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
                System.err.println("Error: " + ex.getMessage());
            }
        }
    }

    public static void printString(Object s) {
        try {
            System.out.println(new String(s.toString().getBytes("windows-1251"), "windows-1252"));
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }
    }
}

