package task1;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by ispiridonova on 25.11.2015.
 */
public class ManagerRouteTransport {
    private final String ROUTE_TRANSPORT_ID = "route_transport_id";
    private final String ROUTE_ID = "route_id";
    private final String TRANSPORT_ID = "transport_id";

    private ArrayList<RouteTransport> routeTransportList = new ArrayList<RouteTransport>();
    private Actions test = new Actions();
    private Statement stmt = null;
    private ResultSet rs = null;


    public ManagerRouteTransport(Connection con) throws SQLException {
        stmt = con.createStatement();
    }

    public void getAllRouteTransport() throws SQLException {
        rs = stmt.executeQuery("SELECT * FROM route_transport");
        while (rs.next()) {
            routeTransportList.add(new RouteTransport(rs.getInt(ROUTE_TRANSPORT_ID), rs.getInt(ROUTE_ID), rs.getInt(TRANSPORT_ID)));
            //  System.out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4));
        }
        test.testRouteTransport(routeTransportList);
    }

}
