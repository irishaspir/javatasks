package task1;

import java.sql.Time;

/**
 * Created by Ira on 21.11.2015.
 */
public class Route {

    private int id;
    private String type;
    private String title;
    private Time startTime;
    private Time  finishTime;

    public Route(int id, String type, String title, Time startTime, Time finishTime){
        this.id = id;
        this.type = type;
        this.title = title;
        this.startTime = startTime;
        this.finishTime = finishTime;

    }
    public Time getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Time finishTime) {
        this.finishTime =finishTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }


}
