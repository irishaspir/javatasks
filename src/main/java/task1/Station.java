package task1;

/**
 * Created by Ira on 21.11.2015.
 */
public class Station {

    private int id;
    private String title;
    private int geoW;
    private int geoN;

    public Station(int id, String title, int geoW, int geoN){
        this.id = id;
        this.title = title;
        this.geoN = geoN;
        this.geoW = geoW;
    }

    public int getGeoN() {
        return geoN;
    }

    public void setGeoN(int geoN) {
        this.geoN = geoN;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getGeoW() {
        return geoW;
    }

    public void setGeoW(int geoW) {
        this.geoW = geoW;
    }

}
