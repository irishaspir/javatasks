package task1;

/**
 * Created by Ira on 21.11.2015.
 */
public class RouteStation {
    private int id;
    private int routeId;
    private int stationId;
    private int stationOder;
    private int distance;

    public RouteStation(int id, int routeId, int stationId, int stationOder, int distance){
        this.id = id;
        this.routeId = routeId;
        this.stationId = stationId;
        this.stationOder = stationOder;
        this.distance = distance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public int getStationId() {
        return stationId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    public int getStationOder() {
        return stationOder;
    }

    public void setStationOder(int stationOder) {
        this.stationOder = stationOder;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

}
