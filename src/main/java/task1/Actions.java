package task1;

import task1.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Ira on 24.11.2015.
 */
public class Actions {

    TestJDBC test = new TestJDBC();

    public void testTransport(ArrayList<Transport> list) {

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).getId());
            System.out.println(list.get(i).getSpeed());
            System.out.println(list.get(i).getStatus());
            System.out.println(list.get(i).getType());
        }
    }


    public void testRoute(ArrayList<Route> list) {

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).getId());
            System.out.println(list.get(i).getTitle());
            System.out.println(list.get(i).getFinishTime());
            System.out.println(list.get(i).getStartTime());
            System.out.println(list.get(i).getType());
        }
    }

    public void testStation(ArrayList<Station> list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).getId()  + " " + list.get(i).getTitle() + " " + list.get(i).getGeoN() + " " + list.get(i).getGeoW());
        }
    }

    public void testRouteStation(ArrayList<RouteStation> list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).getId() + " " + list.get(i).getDistance() + " " + list.get(i).getRouteId() + " " + list.get(i).getStationId() + " " + list.get(i).getStationOder());
        }
    }

    public void testRouteTransport(ArrayList<RouteTransport> list) {

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).getId());
            System.out.println(list.get(i).getRouteId());
            System.out.println(list.get(i).getTransportId());

        }
    }

    public void testRouteTimeTable(ArrayList<RouteTimeTable> list) {

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).getId());
            System.out.println(list.get(i).getRouteId());
            System.out.println(list.get(i).getTransportId());
            System.out.println(list.get(i).getStationId());
            System.out.println(list.get(i).getTime());
            System.out.println(list.get(i).getDirection());
        }
    }


}
