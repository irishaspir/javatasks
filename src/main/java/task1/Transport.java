package task1;

import java.lang.ref.SoftReference;

/**
 * Created by Ira on 21.11.2015.
 */
public class Transport {

    private int id;
    private String type;
    private int speed;
    private String status;

    public Transport(int id, String type, int speed, String status) {
        this.id = id;
        this.type = type;
        this.speed = speed;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
