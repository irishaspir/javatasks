package task1;

/**
 * Created by Ira on 21.11.2015.
 */
public class RouteTransport {
    private int id;
    private int routeId;
    private int transportId;

    public RouteTransport(int id, int routeId, int transportId) {
        this.id = id;
        this.routeId = routeId;
        this.transportId = transportId;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public int getTransportId() {
        return transportId;
    }

    public void setTransportId(int transportId) {
        this.transportId = transportId;
    }
}
