package task1;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by Ira on 24.11.2015.
 */
public class ManagerRouteStation {
    private final String ROUTE_STATION_ID = "route_station_id";
    private final String ROUTE_ID = "route_id";
    private final String STATION_ID = "station_id";
    private final String STATION_ODER = "station_oder";
    private final String DISTANCE = "distance";

    private ArrayList<RouteStation> routeStationList = new ArrayList<RouteStation>();
    private Actions test = new Actions();
    private Statement stmt = null;
    private ResultSet rs = null;
    int i = 1;


    public ManagerRouteStation(Connection con) throws SQLException {

        stmt = con.createStatement();
    }

    public void getAllRouteStation() throws SQLException {
        rs = stmt.executeQuery("SELECT * FROM route_station");
        while (rs.next()) {
            //  routeStationList.add(new RouteStation(rs.getInt(ROUTE_STATION_ID), rs.getInt(ROUTE_ID), rs.getInt(STATION_ID), rs.getInt(STATION_ODER), rs.getInt(DISTANCE)));
            //  System.out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4));
        }
        test.testRouteStation(routeStationList);
    }

    public ArrayList<RouteStation> route(int i) throws SQLException {
        rs = stmt.executeQuery("SELECT * FROM route_station where route_id = '" + i + "' order by station_oder asc" );
        while (rs.next()) {
            routeStationList.add(new RouteStation(rs.getInt(ROUTE_STATION_ID), rs.getInt(ROUTE_ID), rs.getInt(STATION_ID), rs.getInt(STATION_ODER), rs.getInt(DISTANCE)));
        }
        return routeStationList;
    }


}
