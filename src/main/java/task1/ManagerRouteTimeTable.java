package task1;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by Ira on 26.11.2015.
 */
public class ManagerRouteTimeTable {
    private final String ROUTE_TIME_TABLE_ID = "route_time_table_id";
    private final String ROUTE_ID = "route_id";
    private final String STATION_ID = "station_id";
    private final String TRANSPORT_ID = "transport_id";
    private final String TIME = "time";
    private final String DIRECTION = "direction";

    private ArrayList<RouteTimeTable> routeTimeTablesList = new ArrayList<RouteTimeTable>();
    private Actions test = new Actions();
    private Statement stmt = null;
    private ResultSet rs = null;

    public ManagerRouteTimeTable(Connection con) throws SQLException {
        stmt = con.createStatement();
    }

    public void getAllRouteTimeTable() throws SQLException {
        rs = stmt.executeQuery("SELECT * FROM route_time_table");
        while (rs.next()) {
            routeTimeTablesList.add(new RouteTimeTable(rs.getInt(ROUTE_TIME_TABLE_ID), rs.getInt(ROUTE_ID), rs.getInt(STATION_ID), rs.getInt(TRANSPORT_ID), rs.getInt(TIME), rs.getString(DIRECTION)));
        }
        test.testRouteTimeTable(routeTimeTablesList);
    }
}
