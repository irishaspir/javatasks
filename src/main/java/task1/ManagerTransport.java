package task1;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by Ira on 24.11.2015.
 */
public class ManagerTransport {
    private final String TRANSPORT_ID = "transport_id";
    private final String TYPE_TRANSPORT = "type_transport";
    private final String SPEED = "speed";
    private final String STATUS = "status";

    private ArrayList<Transport> transportList = new ArrayList<Transport>();
    private Actions test = new Actions();
    private Statement stmt = null;
    private ResultSet rs = null;
    private Transport tr;

    public ManagerTransport(Connection con) throws SQLException {

        stmt = con.createStatement();
    }

    public void getAllTransport() throws SQLException {
        rs = stmt.executeQuery("SELECT * FROM transport");
        while (rs.next()) {
            transportList.add(new Transport(rs.getInt(TRANSPORT_ID), rs.getString(TYPE_TRANSPORT), rs.getInt(SPEED), rs.getString(STATUS)));
            //  System.out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4));
        }
        test.testTransport(transportList);
    }

    public Transport transport(int transportId) throws SQLException {
        rs = stmt.executeQuery("SELECT * FROM transport where transport_id = '" + transportId +"'");
        while (rs.next()) {
            tr = new Transport(rs.getInt(TRANSPORT_ID), rs.getString(TYPE_TRANSPORT), rs.getInt(SPEED), rs.getString(STATUS));
        }
        return tr;
    }
}
