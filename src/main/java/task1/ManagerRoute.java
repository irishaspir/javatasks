package task1;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by Ira on 24.11.2015.
 */
public class ManagerRoute {
    private final String ROUTE_ID = "route_id";
    private final String TYPE = "type";
    private final String TITlE = "title";
    private final String START_TIME = "start_time";
    private final String  FINISH_TIME = "finish_time";
    private ArrayList<Route> routeList = new ArrayList<Route>();
    private Actions test = new Actions();
    private Statement stmt = null;
    private ResultSet rs = null;
    private Route rt;

    public  ManagerRoute(Connection con) throws SQLException {
        stmt = con.createStatement();
    }


    public void getAllRoute() throws SQLException {
        rs = stmt.executeQuery("SELECT * FROM route");
        while (rs.next()) {
            routeList.add(new Route(rs.getInt(ROUTE_ID), rs.getString(TYPE), rs.getString(TITlE), rs.getTime(START_TIME), rs.getTime(FINISH_TIME)));
            //  System.out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4));
        }
        test.testRoute(routeList);
    }



}
