package task1;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by Ira on 24.11.2015.
 */
public class ManagerStation {
    private final String STATION_ID = "station_id";
    private final String TITLE = "title";
    private final String GEO_W = "geoW";
    private final String GEO_N = "geoN";

    private ArrayList<Station> stationList = new ArrayList<Station>();
    private Actions test = new Actions();
    private Statement stmt = null;
    private ResultSet rs = null;
    private Station st;

    public ManagerStation(Connection con) throws SQLException {

        stmt = con.createStatement();
    }

    public void getAllStation() throws SQLException {
        rs = stmt.executeQuery("SELECT * FROM station");
        while (rs.next()) {
        //    stationList.add(new Station(rs.getInt(STATION_ID), rs.getString(TITLE), rs.getInt(GEO_W), rs.getInt(GEO_N)));
         }
        test.testStation(stationList);
    }

    public Station geoStation(int stationId) throws SQLException {
        rs = stmt.executeQuery("SELECT * FROM station where station_id = '" + stationId + "'");
        while (rs.next()) {
            st = new Station(rs.getInt(STATION_ID), rs.getString(TITLE), rs.getInt(GEO_W), rs.getInt(GEO_N));
        }
        return st;
    }

}
