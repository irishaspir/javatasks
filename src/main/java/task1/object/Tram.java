package task1.object;

/**
 * Created by ispiridonova on 16.10.2015.
 */
public class Tram extends TransportO {

    private int speed = 2;
    private String nameTransport = "Tram #";

    public Tram(String number, int startTimeHour, int startTimeMinutes) {
        super(number,startTimeHour, startTimeMinutes);
        super.setNumber(nameTransport + super.getNumber());
        super.setSpeed(speed);
    }

    public Tram() {
        super.setSpeed(speed);
        super.setNumber(nameTransport);
    }
}
