package task1.object;

/**
 * Created by ispiridonova on 16.10.2015.
 */
public class Trolleybus extends TransportO {
    private int speed = 3;
    private String nameTransport = "Trolleybus #";

    public Trolleybus(String number, int startTimeHour, int startTimeMinutes) {
        super(number, startTimeHour, startTimeMinutes);
        super.setNumber(nameTransport + super.getNumber());
        super.setSpeed(speed);
    }

    public Trolleybus() {
        super.setSpeed(speed);
        super.setNumber(nameTransport);
    }
}
