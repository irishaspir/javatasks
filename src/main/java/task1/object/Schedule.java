package task1.object;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;


/**
 * Created by ispiridonova on 19.10.2015.
 */
public class Schedule {

    private Route route = new Route();
    private Bus bus = new Bus();
    private Tram tram = new Tram();
    private Trolleybus trolleybus = new Trolleybus();
    private Movement one = new Movement();

    /**
     * ������� �������� � �������� �����������
     */
    public void movementSchedule() {
        System.out.println("-------Bus schedule----------" + "\n");
        printSchedule(bus, route.route1());
        printSchedule(bus, route.route2());
        printSchedule(bus, route.route3(), 1);
        System.out.println("\n" + "-------Tram schedule----------" + "\n");
        printSchedule(tram, route.route1(), 1);
        printSchedule(tram, route.route2());
        printSchedule(tram, route.route3());
        System.out.println("\n" + "-------Trolleybus schedule----------" + "\n");
        printSchedule(trolleybus, route.route1());
        printSchedule(trolleybus, route.route2(),1);
        printSchedule(trolleybus, route.route3());
    }


    /**
     * ��������� ���� � ����������� �� ��������
     *
     * @param transport ���������
     * @param route     �������
     * @return ���� � ����������� �� ��������
     */
    private ArrayList<TransportO> listRoute(TransportO transport, Route route) {
        ArrayList<TransportO> schedule = new ArrayList<TransportO>();
        int speed = transport.getSpeed();
        int time = one.totalTime(speed, route);
        int account = route.getAccount();
        int startHour = route.getStartHour();
        int startMinutes = route.getStartMinutes();
        boolean status = true;

        if (account > 1) {
            int shiftTimeMinutes = time / account;
            for (int i = 0; i < account; i++) {
                int minutes = startMinutes + shiftTimeMinutes * i;
                int h = minutes / 60;
                int hour = startHour + h;
                if (transport.getClass().getName().equals(Bus.class.getName()))
                    schedule.add(new Bus("1" + RandomStringUtils.randomNumeric(2), hour, (minutes - h * 60)));
                if (transport.getClass().getName().equals(Tram.class.getName()))
                    schedule.add(new Tram("2" + RandomStringUtils.randomNumeric(3), hour, (minutes - h * 60)));
                if (transport.getClass().getName().equals(Trolleybus.class.getName()))
                    schedule.add(new Trolleybus("3" + RandomStringUtils.randomNumeric(2), hour, (minutes - h * 60)));
            }
        } else {
            if (transport.getClass().getName().equals(Bus.class.getName()))
                schedule.add(new Bus("1" + RandomStringUtils.randomNumeric(1), startHour, startMinutes));
            if (transport.getClass().getName().equals(Tram.class.getName()))
                schedule.add(new Tram("2" + RandomStringUtils.randomNumeric(2), startHour, startMinutes));
            if (transport.getClass().getName().equals(Trolleybus.class.getName()))
                schedule.add(new Trolleybus("3" + RandomStringUtils.randomNumeric(1), startHour, startMinutes));
        }
        return schedule;
    }

    /**
     * ������� ������ � ���������� ��������� �� ��������
     *
     * @param transport ���������
     * @param route     �������
     */
    private void printSchedule(TransportO transport, Route route) {

        for (int i = 0; i < listRoute(transport, route).size(); i++) {
            System.out.println(one.move(listRoute(transport, route).get(i), route));
        }
        System.out.println("--------------------------------------------------");
    }

    /**������� ������ � ���������� ��������� �� �������� � ������ ����������� ���������� ����������
     *
     * @param transport ���������
     * @param route �������
     * @param accountBroken ����������� ���������� �� �������� ����������
     */
    private void printSchedule(TransportO transport, Route route, int accountBroken) {
        if (accountBroken != 0) {
            int account = route.getAccount() - accountBroken;
            route.setAccount(account);
            if (route.getAccount() != 0) {
                printSchedule(transport, route);
                System.out.println("Number of broken transportation is " + accountBroken + " on route " + route.getName());
                System.out.println("--------------------------------------------------");
            } else {
                System.out.println("All transportation is broken on route");
                System.out.println("--------------------------------------------------");
            }
        }
    }
}
