package task1.object;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Ira on 15.10.2015.
 */
public class Route extends Distance {

    Route(int startHour, int startMinutes,int account,String name, Map distance){
        super(startHour,startMinutes,account, name,distance);
    }
    Route(){}

    public Route route1() {
        Route route = new Route(6,0,1, "Route #1", new LinkedHashMap<String, Integer>() {{
            put("Station1", 10);
            put("Station2", 20);
            put("Station3", 55);
            put("Station4", 50);
            put("Station5", 15);
            put("Station6", 40);
        }});
        return route;
    }


    public Route route2() {
        Route route = new Route(7,0,2, "Route #2", new LinkedHashMap<String, Integer>() {{
        put("Station1", 50);
        put("Station2", 25);
        put("Station3", 50);
        put("Station4", 15);
        put("Station5", 30);
        put("Station6", 40);
    }});
        return route;
    }

    public Route route3() {
        Route route = new Route(8,0,3, "Route #3", new LinkedHashMap<String, Integer>() {{
        put("Station1", 5);
        put("Station2", 25);
        put("Station3", 50);
        put("Station4", 5);
        put("Station5", 30);
        put("Station6", 40);
    }});
    return route;
}



}
