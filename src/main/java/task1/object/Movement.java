package task1.object;

import java.util.Map;
import java.util.Set;

/**
 * Created by Ira on 15.10.2015.
 */
public class Movement {

    /**
     * ��������� ������ � ��������� �������� ��������� �� ��������
     *
     * @param transport ���������
     * @param route     �������
     * @return ������ � � ��������� �������� ��������� �� ��������
     */
    public String move(TransportO transport, Distance route) {

        int timeHour = transport.getStartHour();
        int timeMinutes = transport.getStartMinutes();
        int speed = transport.getSpeed();

        String str = route.getName() + " - " + transport.getNumber() + "\n";
        str += "Route " + "starts: " + timeHour + ":" + To60name(timeMinutes) + "\n";

        Set<Map.Entry<String, Integer>> set = route.getDistance().entrySet();
        for (Map.Entry<String, Integer> me : set) {
            timeMinutes = timeMinutes + me.getValue() / speed;

            if (timeMinutes < 60) {
                str += me.getKey() + ": " + timeHour + ":" + To60name(timeMinutes) + "  ";
            } else {

                int a = timeMinutes / 60;
                timeMinutes = timeMinutes - a * 60;
                timeHour = timeHour + a * 1;

                str += me.getKey() + ": " + timeHour + ":" + To60name(timeMinutes) + "  ";
            }
        }

        int totalTime = totalTime(transport.getSpeed(), route);
        int hour = totalTime / 60;
        int minutes = totalTime - hour * 60;
        str += " Total time: " + hour + ":" + To60name(minutes) + "\n";
        return str;
    }

    /**
     * ������� ����������� ����� ����� �� �������� � ����������� �� �������� ����������
     *
     * @param speed �������� ����������
     * @param route �������
     * @return ����� ����� �� ��������  � ����������� �� �������� ����������
     */
    public int totalTime(int speed, Distance route) {
        int time = 0;
        Set<Map.Entry<String, Integer>> set = route.getDistance().entrySet();
        for (Map.Entry<String, Integer> me : set) {
            time = time + me.getValue() / speed;
        }
        return time;
    }

    /**
     * ������� ������������� ������ �������, ���� ������ ������ 10
     *
     * @param x ������
     * @return ����������������� �����
     */
    private String To60name(int x) {
        if (x < 10)
            return "0" + x;
        else
            return "" + x;
    }
}
