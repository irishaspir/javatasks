package task1.object;


/**
 * Created by Ira on 15.10.2015.
 */
abstract class TransportO {

    private int speed;
    private String number;
    private int startHour;
    private int startMinutes;

    public TransportO(String number, int startHour, int startMinutes) {
        this.number=number;
        this.startHour = startHour;
        this.startMinutes = startMinutes;
    }

    public TransportO() {
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getStartHour() {
        return startHour;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public int getStartMinutes() {
        return startMinutes;
    }

    public void setStartMinutes(int startMinutes) {
        this.startMinutes = startMinutes;
    }
}



