package task1.object;

/**
 * Created by Ira on 15.10.2015.
 */
public class Bus extends TransportO {

    private int speed = 1;
    private String nameTransport = "Bus #";

    public Bus(String number, int startHour, int startMinutes) {
        super(number, startHour, startMinutes);
        super.setNumber(nameTransport + super.getNumber());
        super.setSpeed(speed);
    }

    public Bus(){
        super.setSpeed(speed);
        super.setNumber(nameTransport);
    }
}
