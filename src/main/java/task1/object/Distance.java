package task1.object;

import java.util.Map;

/**
 * Created by ispiridonova on 19.10.2015.
 */
public class Distance {

    private String name;
    private int account;
    private Map<String, Integer> distance;
    private int startHour;
    private int startMinutes;

    Distance(int startHour, int startMinutes, int account, String name, Map distance) {
        this.name = name;
        this.distance = distance;
        this.account = account;
        this.startHour = startHour;
        this.startMinutes = startMinutes;
    }

    Distance() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Integer> getDistance() {
        return distance;
    }

    public void setDistance(Map<String, Integer> distance) {
        this.distance = distance;
    }

    public int getAccount() {
        return account;
    }

    public void setAccount(int account) {
        this.account = account;
    }

    public int getStartHour() {
        return startHour;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public int getStartMinutes() {
        return startMinutes;
    }

    public void setStartMinutes(int startMinutes) {
        this.startMinutes = startMinutes;
    }

}
